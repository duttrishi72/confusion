'use strict';

module.exports = function (grunt) {
    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Automatically load required Grunt tasks
    require('jit-grunt')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({
        // TASK 1: sass - converts the scss files to css
        sass: {
            dist: {
                files: {
                    'css/styles.css': 'css/styles.scss'
                }
            }
        },  
        // TASK 2: watch: automating scss => css task
        watch: {
            files: 'css/*.scss',
            tasks: ['sass']
        },
        // TASK 3: browesersync: description pending
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: "./"
                    }
                }
            }
        }  
    });
    // task 'css'
    grunt.registerTask('css', ['sass']);
    // task 'default' => runs browsersync and watch
    grunt.registerTask('default', ['browserSync', 'watch']);

};
